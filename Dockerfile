FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app
COPY ./ ./
#COPY ./.env ./Demo.Api/.env
RUN dotnet tool install --global dotnet-ef
#ENV PATH="${PATH}:/root/.dotnet/tools"
#RUN dotnet ef database update --project Demo.Infrastructure --startup-project Demo.Api
RUN dotnet publish --configuration Release

FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY ./.env ./.env
ENV ASPNETCORE_ENVIRONMENT=Production
ENV DOTNET_PRINT_TELEMETRY_MESSAGE=false
COPY --from=build-env /app/Demo.Api/bin/Release/net6.0 .
EXPOSE 80 
ENTRYPOINT ["dotnet", "Demo.Api.dll"]

#COPY Demo.GraphQL/Demo.Api/bin/Release/net6.0/ App/
#WORKDIR /App
#
#ENV DOTNET_EnableDiagnostics=0
#
#RUN dotnet tool install --global dotnet-ef
#
#RUN dotnet publish --configuration Release
#
#ENTRYPOINT ["dotnet", "Demo.Api.dll"]
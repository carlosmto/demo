﻿
using Demo.Core.Interfaces;

namespace Demo.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {


        void SaveChanges();

        Task SaveChangesAsync();

    }
}

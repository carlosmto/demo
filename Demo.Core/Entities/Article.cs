namespace Demo.Core.Entities
{
    public class Article : BaseEntity
    {
        public string Title { get; set; }
        
        public string Content { get; set; } 
    }
}
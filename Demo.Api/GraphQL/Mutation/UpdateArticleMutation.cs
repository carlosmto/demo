
using HotChocolate.AspNetCore.Authorization;
using System.Security.Claims;

namespace Auth.Api.GraphQL.Mutation;

public record UpdateArticleInput(Guid ArticleId, string? Title = "", string? Content = "");

public record UpdateArticlePayload(string message);

[ExtendObjectType(OperationTypeNames.Mutation)]
public class UpdateArticleMutation
{
    public async Task<UpdateArticlePayload> UpdateArticle(
        UpdateArticleInput input,   DemoContext context)
    {
        
        var article = context.Articles.FirstOrDefault(b => b.Id == input.ArticleId);

        if (article == null) {
            throw new GraphQLException("Articulo no existe");
        }

        if (input.Title != "") {
            article.Title = input.Title;
        }
       
        if (input.Content != "") {
            article.Content = input.Content;
        }
         
        context.Articles.Update(article);

        await context.SaveChangesAsync();
        
        return new UpdateArticlePayload("Se actualizo el articulo");
    }
}



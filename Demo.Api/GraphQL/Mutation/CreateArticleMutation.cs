
using HotChocolate.AspNetCore.Authorization;
using System.Security.Claims;

namespace Auth.Api.GraphQL.Mutation;

public record CreateArticleInput(string Title, string? Content = "");

public record CreateArticlePayload(string message);

[ExtendObjectType(OperationTypeNames.Mutation)]
public class CreateArticleMutation
{
    public async Task<CreateArticlePayload> CreateArticle(
        CreateArticleInput input, DemoContext context)
    {
        
        var exists = context.Articles.Any(b => b.Title == input.Title);

        if (exists) {
            throw new GraphQLException("Titulo del articulo ya existe");
        }

        var article = new Article {
            Title = input.Title,
            Content = input.Content
        };

        context.Articles.Add(article);

        await context.SaveChangesAsync();
        
        return new CreateArticlePayload("Se creo el articulo");
    }
}



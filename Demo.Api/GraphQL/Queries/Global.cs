﻿namespace Demo.Api.GraphQL.Queries
{
    public class Global
    {
        public List<Article> GetListArticles( DemoContext context)
        {
            var articles = context.Articles.ToList();
            return articles;
        }
    }
}

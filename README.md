# Install Demo
dotnet new -i .

# Uninstall Demo
dotnet new --uninstall .

# Create Project with Demo
dotnet new msgql --db Product -n Product

# Install EF
dotnet tool install --global dotnet-ef


# Create DbContext with EF & PostgreSQL Folder API
create .env for test


DB_NAME=demo-cs-staging

DB_HOST=localhost

DB_PORT=5432

DATABASE_USER=user-demo

DATABASE_PASSWORD=t3mpl4t3

